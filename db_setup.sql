DROP TABLE IF EXISTS `clients`;
DROP TABLE IF EXISTS `users`;

CREATE TABLE `clients` (
    `id`        int(11) unsigned NOT NULL AUTO_INCREMENT,
    `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `url`       varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `users` (
    `id`             int(10) unsigned NOT NULL AUTO_INCREMENT,
    `email`          varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `password`       varchar(60) COLLATE utf8_unicode_ci NOT NULL,
    `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `users_email_unique` (`email`)
) AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- INSERT SOME DUMMY CLIENTS
INSERT INTO `clients` VALUES (1,'global_inventory_v00.00.01','http://cs.carsforsale.com:23412'),
                             (2,'data_compendium_v00.00.01','http://cs.carsforsale.com:23512'),
                             (3,'lead_conduit_v00.00.01','http://cs.carsforsale.com:23612'),
                             (4,'dealer_spider_v00.00.01','http://cs.carsforsale.com:23712');
-- INSERT A DUMMY USER
INSERT INTO `users` VALUES (1,'olson.levi@gmail.com','$2a$10$5MXnoV6A2LLOtd4JCyZS3ODKrOppr3puTEm6guXUAxPQcupNv1eqq',NULL);